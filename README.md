# dna-api
API to detect DNA Mutations

## Requirements
------
 - Docker and Docker Compose

## Usage
------
### Run Server

To run server you must run the following command:
```
docker compose up
```
And open ```http://localhost:3000```

### Run Tests

To run tests you must run the following command:
```
docker compose -f docker-compose.test.yml up
```
> **NOTE:** You must close any connection related to 5432 first, if not, tests won't run


## Troubleshooting
------

- Docker run out of space: https://medium.com/@wlarch/no-space-left-on-device-when-using-docker-compose-why-c4a2c783c6f6#:~:text=TLDR%3B%20It%20is%20necessary%20to,do%20a%20complete%20Docker%20cleanup.