FROM node:16
RUN apt-get update && \
	apt-get install -y netcat-openbsd && \
	apt-get clean && \
	rm -fr /var/lib/apt/lists/* /tmp/*
RUN npm install -g node-gyp

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN yarn install
COPY . /usr/src/app

EXPOSE 3000

CMD ["npm", "run", "start:nodemon"]
