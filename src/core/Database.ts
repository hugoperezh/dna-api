import { Sequelize } from 'sequelize'
import { DB_URI } from '../config';

const sequelizeConnection = new Sequelize(DB_URI!, {
    logging: false
});

export default sequelizeConnection;