import sequelizeConnection from '../core/Database';
import Dna from '../models/DnaModel';
export default class StatsService {
    /**
     * Get DNA Stats
     * @returns 
     */
    public static async getStats() {
        let data: Dna[] | { [key: string]: any }[] ;
        const result = {
            count_mutations: 0,
            count_no_mutation: 0,
            ratio: 0
        }
        try {
            // Query to get counts of data based 'is_mutation' column
            data = await Dna.findAll({
                attributes: [
                    'is_mutation',
                    [sequelizeConnection.fn('COUNT', sequelizeConnection.col('is_mutation')), 'count']
                ],
                group: 'is_mutation',
                raw: true
            }) as unknown as { [key: string]: any }[];
            data.forEach(i => {
                if (i.is_mutation) {
                    result['count_mutations'] = parseInt(i.count);
                } else {
                    result['count_no_mutation'] = parseInt(i.count);
                }
            });
        } catch (error) {
            return {
                error
            };
        }
        result.ratio = this._calculateRatio(result['count_mutations'], result['count_no_mutation']);
        return result;
    }

    /**
     * Clear DNA table
     * @returns Promise
     */
    public static async clearData(): Promise<void> {
        await Dna.destroy({
            where: {}
        });
    }

    /**
     * Get ratio
     * @param numA number
     * @param numB  number
     * @returns number
     */
    private static _calculateRatio(numA: number, numB: number): number {
        if ((isNaN(numA) || (!isNaN(numA) && numA <= 0)) || (isNaN(numB) || (!isNaN(numB) && numB <= 0))) {
            return 0;
        }
        let num;
        for(num = numB; num >1; num--) {
            if((numA % num) == 0 && (numB % num) == 0) {
                numA = numA/num;
                numB = numB/num;
            }
        }
        return numA/numB;
    }
}
