import { Request, Response, NextFunction } from 'express';
import Dna from '../services/DnaService';
export default class RequestService {
    public static verifyDna(
        req: Request,
        res: Response,
        next: NextFunction
    ): void {
        // Verify if property exists in the request body
        if (
            !req.body ||
            (req.body &&
                (!Object.keys(req.body) ||
                    (Object.keys(req.body) &&
                        !Object.hasOwnProperty.call(req.body, 'dna'))))
        ) {
            res.status(400).json();
            return;
        }
        const dna = req.body['dna'];
        // Verify that DNA is an Array
        if (!Array.isArray(dna)) {
            res.status(400).json();
            return;
        }
        // Verify that Array has length of 4 or more
        if (dna.length < 4) {
            res.status(400).json();
            return;
        }
        // Verify that every item in the Array only has 'TACG'
        const reg = new RegExp(/^[TACG]+$/);
        if (!dna.every((i: string) => reg.test(i))) {
            res.status(400).json();
            return;
        }
        // Validate that the items has the the same length as the array to ensure that satisfy NxN
        if (
            !dna.every(
                (i: string) => typeof i === 'string' && i.length === dna.length
            )
        ) {
            res.status(400).json();
            return;
        }
        next();
    }

    /**
     * Verify existing DNA in DB
     * @param req Request 
     * @param res Response
     * @param next NextFunction
     * @returns Promise
     */
    public static async verifyRepeatedDna(req: Request, res: Response, next: NextFunction): Promise<void> {
        const dna = new Dna(req.body['dna']);
        const data = await dna.getDb();
        if (data) {
            if (data.is_mutation) {
                res.status(200).end();
            } else {
                res.status(403).end();
            }
            return;
        }
        next();
    }
}