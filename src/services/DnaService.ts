import Dna from '../models/DnaModel';
import crypto from 'crypto';

export default class DnaService {
    public dna: string[];
    public mutation = false;
    constructor(dna: string[], mutation = false) {
        this.dna = dna;
        this.mutation = mutation;
    }

    /**
     * Verify mutations
     * @returns Promise<boolean>
     */
    public async hasMutation(): Promise<boolean> {
        // Get sequences with mutation
        const rows = this._checkRows(this.dna);
        const columns = this._checkColumns(this.dna);
        const diagonal = this._checkDiagonal(this.dna);
        // Verify sequences per method and sum all of them
        const result =
            rows > 1 ||
            columns > 1 ||
            diagonal > 1 ||
            rows + columns + diagonal > 1;
        this.mutation = result;
        try {
            await this.create();
        } catch (error) {
            console.log(error);
        }
        return result;
    }

    /**
     * Add DNA in DB
     * @returns Object
     */
    public async create(): Promise<any> {
        try {
            await Dna.create({
                id: crypto.randomBytes(16).toString('hex'),
                dna: this.dna,
                is_mutation: this.mutation,
            });
        } catch (e) {
            console.log(e);
            return { message: 'An error occured', success: false };
        }
    }

    /**
     * Look for an specific DNA
     * @returns Object | null | undefined
     */
    public async getDb(): Promise<Dna | null | undefined> {
        let res;
        try {
            res = await Dna.findOne({
                where: {
                    dna: this.dna,
                },
            });
        } catch (error) {
            console.log(error);
        }
        return res;
    }

    /**
     * Verify mutation by checking Rows
     * @param dna string[]
     * @returns number
     */
    private _checkRows(dna: string[]): number {
        let sequence = 0;
        for (let i = 0; i < dna.length; i++) {
            for (let j = 0; j < dna[i].length; j++) {
                if (
                    dna[i][j] &&
                    dna[i][j + 1] &&
                    dna[i][j + 2] &&
                    dna[i][j + 3]
                ) {
                    if (
                        dna[i][j] === dna[i][j + 1] &&
                        dna[i][j] === dna[i][j + 2] &&
                        dna[i][j] === dna[i][j + 3]
                    ) {
                        sequence++;
                    }
                    if (sequence > 1) {
                        break;
                    }
                }
            }
            if (sequence > 1) {
                break;
            }
        }
        return sequence;
    }

    /**
     * Verify mutation by checking Columns
     * @param dna string[]
     * @returns number
     */
    private _checkColumns(dna: string[]): number {
        let sequence = 0;
        for (let i = 0; i < dna.length; i++) {
            for (let j = 0; j < dna[i].length; j++) {
                if (dna[j] && dna[j + 1] && dna[j + 2] && dna[j + 3]) {
                    if (
                        dna[j][i] === dna[j + 1][i] &&
                        dna[j][i] === dna[j + 2][i] &&
                        dna[j][i] === dna[j + 3][i]
                    ) {
                        sequence++;
                    }
                    if (sequence > 1) {
                        break;
                    }
                }
            }
            if (sequence > 1) {
                break;
            }
        }
        return sequence;
    }

    /**
     * Verify mutation by checking diagonals
     * @param dna string[]
     * @returns number
     */
    private _checkDiagonal(dna: string[]): number {
        let sequence = 0;
        let step = 0;
        const options: { [key: number]: string[] } = {
            0: dna,
            1: dna.map((i) => i.split('').reverse().join('')),
            2: dna.map((i) => i.split('').join('')).reverse(),
            3: dna.map((i) => i.split('').reverse().join('')).reverse(),
        };
        // Verify mutation from left to right, right to left and reversed
        while (sequence < 2 && step < 4) {
            let dnaArray = [];
            dnaArray = options[step];
            sequence += this._itirateDiagonal(dnaArray);
            step++;
        }
        return sequence;
    }

    private _itirateDiagonal(dna: string[]): number {
        let n = 0;
        for (let i = 0; i < dna.length; i++) {
            for (let j = 0; j < dna[i].length; j++) {
                if (
                    dna[i] &&
                    dna[i][j] &&
                    dna[i + 1] &&
                    dna[i + 1][j + 1] &&
                    dna[i + 2] &&
                    dna[i + 2][j + 2] &&
                    dna[i + 3] &&
                    dna[i + 3][j + 3]
                ) {
                    if (
                        dna[j][i] === dna[j + 1][i] &&
                        dna[j][i] === dna[j + 2][i] &&
                        dna[j][i] === dna[j + 3][i]
                    ) {
                        n++;
                    }
                    if (n > 1) {
                        break;
                    }
                }
            }
            if (n > 1) {
                break;
            }
        }

        return n;
    }
}
