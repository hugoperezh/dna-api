import Dna from './DnaModel';
const dbInit = () => Promise.all([
    Dna.sync({ alter: true })
]);

export default dbInit;