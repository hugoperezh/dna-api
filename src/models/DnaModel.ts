/* eslint-disable @typescript-eslint/no-empty-interface */

import { DataTypes, Model, Optional } from 'sequelize';
import sequelizeConnection from '../core/Database';
import crypto from "crypto";
import { IDna } from '../typings/interfaces/Dna';

export interface DnaInput extends Optional<IDna, 'id'> {}

class Dna extends Model<IDna, DnaInput> implements IDna {
    public id!: string;
    public dna!: string[];
    public is_mutation!: boolean;
    
    // timestamps!
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}

Dna.init({
    id: {
        type: DataTypes.STRING,
        autoIncrement: false,
        primaryKey: true,
        defaultValue: crypto.randomBytes(16).toString("hex")
    },
    dna: {
        type: DataTypes.ARRAY(DataTypes.STRING),
        allowNull: false
    },
    is_mutation: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
}, {
    underscored: true,
    sequelize: sequelizeConnection,
    paranoid: false
})

export default Dna;