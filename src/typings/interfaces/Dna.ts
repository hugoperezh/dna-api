export interface IDna {
    id: string;
    dna: string[];
    is_mutation: boolean;
}