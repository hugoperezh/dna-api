import { Application, RequestHandler } from 'express';
import { Sequelize } from 'sequelize';
import http from 'http';
import Controller from './Controller';

export default class Server {
    private app: Application;
    private database: Sequelize | null;
    private readonly port: number;

    constructor(app: Application, database: Sequelize | null, port: number) {
        this.app = app;
        this.database = database;
        this.port = port;
    }

    /**
     * Runs server
     * @returns listen
     */
    public run(): http.Server {
        return this.app.listen(this.port, () => {
            console.log(`The server is running on port ${this.port}`);
        });
    }

    /**
     * Gets app instance
     * @returns app 
     */
    public getApp(): Application {
        return this.app;
    }

    /**
     * Set middlewares
     * @param middlewares 
     */
    public loadMiddleware(middlewares: Array<RequestHandler>): void {
        middlewares.forEach((middleware) => {
            this.app.use(middleware);
        });
    }

    /**
     * Set controllers
     * @param controllers 
     */
    public loadControllers(controllers: Array<Controller>): void {
        controllers.forEach((controller) => {
            this.app.use(controller.path, controller.setRoutes());
        });
    }
}