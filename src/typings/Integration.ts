/*
 This is a duplicated version of app.ts to run tests
*/
// Imports
import express, { Application, RequestHandler } from 'express';
import Server from './Server';
import Controller from './Controller';
import Stats from '../services/StatsService';
import {
    DnaController,
    StatsController
} from '../controllers';
import { json, urlencoded } from 'body-parser';
import cors from 'cors';
import { PORT } from '../config';
import dbInit from '../models';

const app: Application = express();
const server: Server = new Server(app, null, PORT);

const controllers: Array<Controller> = [
    new DnaController(),
    new StatsController()
];

const globalMiddleware: Array<RequestHandler> = [
    urlencoded({ extended: false }),
    json(),
    cors({ credentials: true, origin: true })
];


const appServer = async() => {
    // eslint-disable-next-line no-async-promise-executor
    return new Promise(async (resolve) => {
        await dbInit();
        await Stats.clearData();
        server.loadMiddleware(globalMiddleware);
        server.loadControllers(controllers);
        resolve(server.getApp());
    });
}
export default appServer;