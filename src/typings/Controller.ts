import { Response, Request, NextFunction, Router } from 'express';

export enum Methods {
    ALL = 'all',
    GET = 'get',
    POST = 'post',
    PUT = 'put',
    DELETE = 'delete',
    PATCH = 'patch',
    OPTIONS = 'options',
    HEAD = 'head',
}

interface IRoute {
    path: string;
    method: Methods;
    handler: (
        req: Request,
        res: Response,
        next: NextFunction
    ) => void | Promise<void>;
    localMiddleware: ((
        req: Request,
        res: Response,
        next: NextFunction
    ) => void)[];
}

export default abstract class Controller {
    public router: Router = Router();
    public abstract path: string;
    protected abstract readonly routes: Array<IRoute>;

    /**
     * Initialize routes
     * @returns Router
     */
    public setRoutes = (): Router => {
        for (const route of this.routes) {
            for (const mw of route.localMiddleware) {
                this.router.use(route.path, mw);
            }
            try {
                this.router[route.method](route.path, route.handler);
            } catch (err) {
                console.error('not a valid method');
            }
        }

        return this.router;
    };
    
    /**
     * Method to return success response
     * @param res 
     * @param data 
     * @param message 
     * @returns Response<any, Record<string, any>>
     */
    protected sendSuccess(
        res: Response,
        data?: object | any[],
        message?: string
    ): Response {
        let _response = {};
        if (data) {
            _response = data;
        }
        return res.status(200).json(_response);
    }

    /**
     * Method to return fail response
     * @param res 
     * @param code 
     * @param message 
     * @returns 
     */
    protected sendError(res: Response, code = 500, message?: string): Response {
        return res.status(code).end();
    }
}