export const PORT = parseInt(process.env.PORT!); // Port that express will listen on
export const DB_URI = process.env.POSTGRES_URI!; // DB URI