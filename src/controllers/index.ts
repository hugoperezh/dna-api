import DnaController from './DnaController';
import StatsController from './StatsController';

export {
    DnaController,
    StatsController
}