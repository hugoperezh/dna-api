import { Request, Response } from 'express';
import Controller, { Methods } from '../typings/Controller';
import Dna from '../services/DnaService';
import RequestService from '../services/RequestService';

export default class DnaController extends Controller {
    path = '/mutation';
    routes = [
        {
            path: '/',
            method: Methods.POST,
            handler: this.checkDnaMutation,
            localMiddleware: [RequestService.verifyDna, RequestService.verifyRepeatedDna],
        },
    ];

    /**
     * POST /mutation - Verify mutation
     * @param req 
     * @param res 
     * @returns 
     */
    async checkDnaMutation(
        req: Request,
        res: Response
    ): Promise<void> {
        try {
            const dna = new Dna(req.body['dna']);
            const result = await dna.hasMutation();
            if (result) {
                super.sendSuccess(res);
                return;
            }
            super.sendError(res, 403);
        } catch (e) {
            console.log(e);
            super.sendError(res);
        }
    }
}