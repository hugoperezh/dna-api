import request from 'supertest'
import appServer from '../../typings/Integration';
import Stats from '../../services/StatsService'; 
let server: any;

beforeAll(async () => {
    server = await appServer();
});
beforeEach((): void => {
  jest.setTimeout(60000);
});

describe('POST /mutation', () => {
    beforeAll(async() => {
      await Stats.clearData();
    });
    it('should return 400 when send no data', (done) => {
      jest.setTimeout(60000);
      request(server)
        .post(`/mutation`)
        .send({})
        .expect(400)
        .end((err, res) => {
          if (err) return done(err)
          done();
        })
    });
    it('should return 400 when send an array with length less than 4', (done) => {
      jest.setTimeout(60000);
      request(server)
        .post(`/mutation`)
        .send({
          dna: ["ATTAGC", "CAACCC", "TTATAC"] 
        })
        .expect(400)
        .end((err, res) => {
          if (err) return done(err)
          done();
        })
    });
    it('should return 400 when send an array with an invalid character', (done) => {
      jest.setTimeout(60000);
      request(server)
        .post(`/mutation`)
        .send({
          dna: ["ATTAGC", "CAACCC", "TTATAC", "AGAATC", "CHCCTA", "TCACTG"] 
        })
        .expect(400)
        .end((err, res) => {
          if (err) return done(err)
          done();
        })
    });
    it('should return 400 when send an array that dont satisfy NxN', (done) => {
      jest.setTimeout(60000);
      request(server)
        .post(`/mutation`)
        .send({
          dna: ["ATTAC", "CACCC", "TATAC", "GAATC", "CCCTA", "TACTG"] 
        })
        .expect(400)
        .end((err, res) => {
          if (err) return done(err)
          done();
        })
    });
    it('should return 200', (done) => {
      jest.setTimeout(60000);
      request(server)
        .post(`/mutation`)
        .send({
          dna: ["ATTAGC", "CAACCC", "TTATAC", "AGAATC", "CCCCTA", "TCACTG"] })
        .expect(200)
        .end((err, res) => {
          if (err) return done(err)
          done();
        })
    });
    it('should return 403', (done) => {
      jest.setTimeout(60000);
      request(server)
        .post(`/mutation`)
        .send({
          dna: ["ATTAGC", 
                "CAACCA", 
                "TTATAC", 
                "AGAATC", 
                "CACCTA", 
                "TCACTG"] 
        })
        .expect(403)
        .end((err, res) => {
          if (err) return done(err)
          done();
        })
    });
});

afterAll((done) => {
  done();
});