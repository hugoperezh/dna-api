import request from 'supertest';
import appServer from '../../typings/Integration';
import Stats from '../../services/StatsService';
let server: any;

beforeAll(async () => {
    server = await appServer();
});
describe('GET /stats', () => {
    beforeAll(async() => {
        await Stats.clearData();
    });
    it('should return 200 with values of zero', (done) => {
        jest.setTimeout(60000);
        request(server)
            .get(`/stats`)
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                expect(res.body).toMatchObject({
                    count_mutations: 0,
                    count_no_mutation: 0,
                    ratio: 0,
                });
                done();
            });
    });
    it('should return 200', (done) => {
        jest.setTimeout(60000);
        request(server)
            .post(`/mutation`)
            .send({
                dna: [
                    'ATTAGC',
                    'CAACCC',
                    'TTATAC',
                    'AGAATC',
                    'CCCCTA',
                    'TCACTG',
                ],
            })
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                done();
            });
    });
    it('should return 403', (done) => {
        jest.setTimeout(60000);
        request(server)
            .post(`/mutation`)
            .send({
                dna: [
                    'ATTAGC',
                    'CAACCA',
                    'TTATAC',
                    'AGAATC',
                    'CACCTA',
                    'TCACTG',
                ],
            })
            .expect(403)
            .end((err, res) => {
                if (err) return done(err);
                done();
            });
    });
    it('should return 200', (done) => {
        jest.setTimeout(60000);
        request(server)
            .post(`/mutation`)
            .send({
                dna: [
                    'ATTATC',
                    'CAACCC',
                    'TTATAC',
                    'AGAATC',
                    'CCCCTA',
                    'TCACTG',
                ],
            })
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                done();
            });
    });
    it('should return 200 with data', (done) => {
        jest.setTimeout(60000);
        request(server)
            .get(`/stats`)
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                expect(res.body).toMatchObject({
                    count_mutations: 2,
                    count_no_mutation: 1,
                    ratio: 2,
                });
                done();
            });
    });
});

afterAll((done) => {
    done();
});
