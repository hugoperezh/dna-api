import { Request, Response } from 'express';
import Controller, { Methods } from '../typings/Controller';
import Stats from '../services/StatsService';

export default class StatsController extends Controller {
    path = '/stats';
    routes = [
        {
            path: '/',
            method: Methods.GET,
            handler: this.getStats,
            localMiddleware: [],
        },
    ];

    /**
     * GET /stats - Fetch stats of DNA
     * @param req Request
     * @param res  Response
     */
    async getStats(
        req: Request,
        res: Response
    ): Promise<void> {
        let data;
        try {
            data = await Stats.getStats();
            if (Object.hasOwnProperty.call(data, 'error')) {
                super.sendError(res);
            }
            super.sendSuccess(res, data);
        } catch (e) {
            super.sendError(res);
        }
    }
}