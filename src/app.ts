// Imports
import express, { Application, RequestHandler } from 'express';
import Server from './typings/Server';
import Controller from './typings/Controller';
import {
    DnaController,
    StatsController
} from './controllers';
import { json, urlencoded } from 'body-parser';
import cors from 'cors';
import { PORT } from './config';
import dbInit from './models';

const app: Application = express();
const server: Server = new Server(app, null, PORT);

// Init Controllers
const controllers: Array<Controller> = [
    new DnaController(),
    new StatsController()
];

// Init Middleware
const globalMiddleware: Array<RequestHandler> = [
    urlencoded({ extended: false }),
    json(),
    cors({ credentials: true, origin: true })
];

Promise.resolve()
    .then(() => dbInit()) // Init DB
    .then(() => {
        server.loadMiddleware(globalMiddleware);
        server.loadControllers(controllers);
        server.run();
    });